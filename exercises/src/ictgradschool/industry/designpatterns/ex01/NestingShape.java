package ictgradschool.industry.designpatterns.ex01;

import java.util.ArrayList;

public class NestingShape extends Shape {

    ArrayList<Shape> shape = new ArrayList<Shape>();

    public NestingShape() {
        super();
    }

    public NestingShape(int x, int y) {
        super(x, y);
    }

    public NestingShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public NestingShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }

    public void move(int width, int height) {
        super.move(width, height);
        for (Shape childshape : shape) {
            childshape.move(this.fWidth, this.fHeight);
        }
    }

    public void paint(Painter painter) {
        painter.drawRect(this.fX, this.fY, this.fWidth, this.fHeight);
        painter.translate(this.fX, this.fY);
        for (Shape childshape:shape){
            childshape.paint(painter);
        }painter.translate(-this.fX, -this.fY);
    }

    public void add(Shape child) throws IllegalArgumentException {
        if (shape.contains(child)) {
            throw new IllegalArgumentException();
        }
        if (child.fHeight + child.fY >= this.fHeight || child.fWidth + child.fX >= this.fWidth) {
            throw new IllegalArgumentException();
        }
        if (child.parent != null){
            throw new IllegalArgumentException();
        }
        shape.add(child);
        child.setParent(this);
    }

    public void remove(Shape child) {
        if (shape.contains(child)) {
            shape.remove(child);
            child.setParent(null);
        }

    }

    public Shape shapeAt(int index) throws IndexOutOfBoundsException {
        Shape position = null;
        position = shape.get(index);
        return position;
    }

    public int shapeCount() {
        return shape.size();
    }

    public int indexOf(Shape child) {
        return shape.indexOf(child);
    }

    public boolean contains(Shape child) {
        return shape.contains(child);
    }
}
