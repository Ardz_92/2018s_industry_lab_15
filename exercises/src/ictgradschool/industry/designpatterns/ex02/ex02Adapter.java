package ictgradschool.industry.designpatterns.ex02;

import ictgradschool.industry.designpatterns.ex01.NestingShape;
import ictgradschool.industry.designpatterns.ex01.Shape;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.Iterator;

public class ex02Adapter implements TreeModel{

    private Shape adaptee;

    public ex02Adapter(Shape root) {
        adaptee = root;
    }

    @Override
    public Object getRoot() {
        return adaptee;
    }

    @Override
    public Object getChild(Object parent, int index) {
        Object result = null;

        if (parent instanceof NestingShape){
            NestingShape shape = (NestingShape)parent;
            result=shape.shapeAt(index);
        }
        return result;
    }

    @Override
    public int getChildCount(Object parent) {
        int finalCount = 0;

        if (parent instanceof NestingShape){
            NestingShape shape = (NestingShape)parent;
            finalCount=shape.shapeCount();
        }

        return finalCount;
    }

    @Override
    public boolean isLeaf(Object node) {
        return !(node instanceof NestingShape);
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
//not needed
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        int indexOfChild = -1;

        if (parent instanceof NestingShape) {
            NestingShape shape = (NestingShape) parent;
            if (child instanceof Shape) {
                Shape childShape = (Shape) child;
                indexOfChild = shape.indexOf(childShape);
            }
        }
        return indexOfChild;
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
//not needed
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
//not needed
    }
}
